import React, { useState, useEffect } from "react";
import {
    Typography,
    Container
}
from '@material-ui/core';
import { Chart } from 'react-google-charts';

const Dashboard = () => {
    const [salesData, setSalesData ] = useState({});

    useEffect(() => {
        const url = 'https://egrrugiwoi.execute-api.us-east-2.amazonaws.com/dev/salesdata';
            fetch(url)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setSalesData(data);
            });
    }, []);

    return(<div>
        <Container>
        <Chart
            width={'500px'}
            height={'300px'}
            chartType="Table"
            loader={<div>Loading Chart</div>}
            data={salesData}
            options={{
                showRowNumber: true,
            }}
            rootProps={{ 'data-testid': '1' }}
            />
                    <Chart
            width={'500px'}
            height={'300px'}
            chartType="Line"
            loader={<div>Loading Chart</div>}
            data={salesData}
            options={{
                hAxis: {
                  title: 'Time',
                },
                vAxis: {
                  title: 'Popularity',
                },
              }}
            rootProps={{ 'data-testid': '1' }}
            />
        </Container>
        </div>
    )
}

export default Dashboard;