import logo from './logo.svg';
import './App.css';
import Dashboard from './Dashboard';
import {
  AppBar
}
from '@material-ui/core';

function App() {
  return (
    <div className="App">
      <AppBar/>
      <Dashboard />
    </div>
  );
}

export default App;
