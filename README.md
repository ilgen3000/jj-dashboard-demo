# jj-dashboard-demo

## Description
The purpose of this React app is to serve as a reference for building a simple dashboard.

## Install

* Install dependencies

`npm install`

* Start local server

`npm run start`

## Deployment

* Build app

`npm run build`

* Push to AWS S3

`npm run deploy`

## Architecture diagram
![Archictecture](/images/Dashboard Architecture.png)
